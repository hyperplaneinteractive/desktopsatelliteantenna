#include <SatelliteAntenna.h>
#if defined(ARDUINO_SAMD_ZERO) && defined(SERIAL_PORT_USBVIRTUAL)
  // Required for Serial on Zero based boards
  #define Serial SERIAL_PORT_USBVIRTUAL
#endif

SatelliteAntenna sat(Serial1);

void setup() {
  sat.begin();
  sat.Calibrate();
}

void loop() {
  sat.MoveAzimuthNorth();
  delay(5000);
  sat.MoveAzimuthSouth();
  delay(5000);
}

