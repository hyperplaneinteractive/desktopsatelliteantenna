/*
  SatelliteAntenna.h 
  
  Mike Blakemore
  Hyperplane Interactive
*/

#ifndef SatelliteAntenna_h
#define SatelliteAntenna_h

#include "Arduino.h"
#include <HardwareSerial.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_LSM9DS0.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <avr/dtostrf.h>
#include <TinyGPS++.h>

#define VBATPIN A7

class SatelliteAntenna
{
  public:
	  SatelliteAntenna(Uart& s);
	void Calibrate();
	void begin();
    void WaitForVolts(float v);
	void MoveDishSteps(int s);
	void MoveDishEndstop();
	void MoveAzimuthSteps(int steps, bool dir);
	void MoveAzimuthNorth();
	void MoveAzimuthSouth();
	void ActivateDrivers();
    float BatteryLevel();
	String FloatToString(float f);
	void LcdPrint(String text);
	void LcdPrint(String text, String line2);
	String debugMsg = "";
	Adafruit_LSM9DS0 lsm = Adafruit_LSM9DS0();
	Adafruit_SSD1306 display = Adafruit_SSD1306();
	TinyGPSPlus gps;
	String GpsMeters = "";
	String GpsLat = "";
	String GpsLng = "";
  private:
    int _driverLogicPin = 6;
	int _step1Pin = 12;
	int _step2Pin = 10;
	int _dir1Pin = 11;
	int _dir2Pin = 13;
	int _optPin = 5;
	int _mdly = 1500;
	int _maxDishSteps = 5160;
	float _magMinX = 30000;
	float _magMaxX = 0;
	float _magMinY = 30000;
	float _magMaxY = 0;
	float _lastX, _lastY;
	float _battFull = 4.25;
	float GetDir();
	String LcdText = "";
	boolean gpsStatus[7] = { false, false, false, false, false, false, false };
	unsigned long start;
	Uart& gpsSerial;
	void configureUblox(byte* settingsArrayPointer);
	void calcChecksum(byte* checksumPayload, byte payloadSize);
	void sendUBX(byte* UBXmsg, byte msgLength);
	byte getUBX_ACK(byte* msgID);
	void printHex(uint8_t* data, uint8_t length); // prints 8-bit data in hex
	void setBaud(byte baudSetting);
};

#endif

