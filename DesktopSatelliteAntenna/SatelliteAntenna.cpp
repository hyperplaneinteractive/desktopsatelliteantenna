/*
  SatelliteAntenna.cpp

  Mike Blakemore
  Hyperplane Interactive
*/

#include "Arduino.h"
#include "SatelliteAntenna.h"
#include <HardwareSerial.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_LSM9DS0.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <avr/dtostrf.h>

SatelliteAntenna::SatelliteAntenna(Uart& s) :gpsSerial(s)
{

}

void SatelliteAntenna::begin() {
	pinMode(_driverLogicPin, OUTPUT);
	pinMode(_step1Pin, OUTPUT);
	pinMode(_step2Pin, OUTPUT);
	pinMode(_dir1Pin, OUTPUT);
	pinMode(_dir2Pin, OUTPUT);
	pinMode(_optPin, INPUT);
	digitalWrite(_driverLogicPin, LOW);
	digitalWrite(_dir1Pin, LOW);
	digitalWrite(_dir2Pin, LOW);
	digitalWrite(_step1Pin, LOW);
	digitalWrite(_step2Pin, LOW);

	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

	display.clearDisplay();
	display.display();

	
	delay(100);
	gpsSerial.begin(9600);

	//Settings Array contains the following settings: [0]NavMode, [1]DataRate1, [2]DataRate2, [3]PortRateByte1, [4]PortRateByte2, [5]PortRateByte3, 
	//[6]NMEA GLL Sentence, [7]NMEA GSA Sentence, [8]NMEA GSV Sentence, [9]NMEA RMC Sentence, [10]NMEA VTG Sentence
	//NavMode: 
	//Pedestrian Mode    = 0x03
	//Automotive Mode    = 0x04
	//Sea Mode           = 0x05
	//Airborne < 1G Mode = 0x06
	//
	//DataRate:
	//1Hz     = 0xE8 0x03
	//2Hz     = 0xF4 0x01
	//3.33Hz  = 0x2C 0x01
	//4Hz     = 0xFA 0x00
	//
	//PortRate:
	//4800   = C0 12 00
	//9600   = 80 25 00
	//19200  = 00 4B 00  **SOFTWARESERIAL LIMIT FOR ARDUINO UNO R3!**
	//38400  = 00 96 00  **SOFTWARESERIAL LIMIT FOR ARDUINO MEGA 2560!**
	//57600  = 00 E1 00
	//115200 = 00 C2 01
	//230400 = 00 84 03
	//
	//NMEA Messages: 
	//OFF = 0x00
	//ON  = 0x01
	//
	byte settingsArray[] = { 0x03, 0xFA, 0x00, 0x00, 0xE1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; //
    configureUblox(settingsArray);


	// Try to initialise and warn if we couldn't detect the chip
	if (!lsm.begin())
	{
		LcdPrint("Oops ... unable to initialize the LSM9DS0. Check your wiring!");
		while (1);
	}


	// 1.) Set the accelerometer range
	lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
	//lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_4G);
	//lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_6G);
	//lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_8G);
	//lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_16G);

	// 2.) Set the magnetometer sensitivity
	lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
	//lsm.setupMag(lsm.LSM9DS0_MAGGAIN_4GAUSS);
	//lsm.setupMag(lsm.LSM9DS0_MAGGAIN_8GAUSS);
	//lsm.setupMag(lsm.LSM9DS0_MAGGAIN_12GAUSS);

	// 3.) Setup the gyroscope
	lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
	//lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_500DPS);
	//lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_2000DPS);
	for (int i = 0; i < 1000; i++) {
		while (gpsSerial.available() > 0)
			gps.encode(gpsSerial.read());
		if (gps.altitude.isUpdated()) {
			GpsMeters = String(gps.altitude.meters());
			GpsLat = String(gps.location.lat(), 6);
			GpsLng = String(gps.location.lng(), 6);
			
		}
		delay(10);
	}
	LcdPrint("ALT=" + GpsMeters + " LAT=" + GpsLat + " LNG=" + GpsLng);
}

void SatelliteAntenna::Calibrate() {
	WaitForVolts(_battFull);
	MoveDishEndstop();
	MoveDishSteps(2580);
	digitalWrite(_driverLogicPin, LOW);
	delay(200);
	for (int i = 0; i < 300; i++) {
		MoveAzimuthSteps(40, HIGH);
		delay(1);
		float x = 0, y = 0;
		for (int i = 0; i < 4; i++) {
			delay(1);
			lsm.read();
			x += lsm.magData.x;
			y += lsm.magData.y;
		}
		x = x / 4;
		y = y / 4;
		LcdPrint("Calibrating...", "X: " + FloatToString(x) + "  Y: " + FloatToString(y));
		if (x < _magMinX)
		{
			_magMinX = x;
		}
		if (x > _magMaxX)
		{
			_magMaxX = x;
		}
		if (y < _magMinY)
		{
			_magMinY = y;
		}
		if (y > _magMaxY)
		{
			_magMaxY = y;
		}
	}
	LcdPrint("");
}

void SatelliteAntenna::MoveDishSteps(int s) {
	digitalWrite(_driverLogicPin, HIGH);
	digitalWrite(_dir1Pin, LOW);
	delayMicroseconds(_mdly);
	for (int y = 0; y < s; y++) {
		digitalWrite(_step1Pin, LOW);
		delayMicroseconds(_mdly);
		digitalWrite(_step1Pin, HIGH);
		delayMicroseconds(_mdly);
	}
}

void SatelliteAntenna::MoveDishEndstop() {
	digitalWrite(_driverLogicPin, HIGH);
	delay(1);
	int icnt = 0;
	digitalWrite(_dir1Pin, HIGH);
	delayMicroseconds(_mdly);
	while (digitalRead(_optPin) == LOW) {
		digitalWrite(_step1Pin, LOW);
		delayMicroseconds(_mdly);
		digitalWrite(_step1Pin, HIGH);
		delayMicroseconds(_mdly);
		icnt++;
	}
}

void SatelliteAntenna::ActivateDrivers() {
	digitalWrite(_dir1Pin, LOW);
	digitalWrite(_dir2Pin, LOW);
	digitalWrite(_step1Pin, LOW);
	digitalWrite(_step2Pin, LOW);
	digitalWrite(_driverLogicPin, HIGH);
	delay(1);
	delayMicroseconds(_mdly);
	while (digitalRead(_optPin) == LOW) {
		delayMicroseconds(_mdly);
	}
	digitalWrite(_driverLogicPin, LOW);
}

void SatelliteAntenna::MoveAzimuthSteps(int steps, bool dir) {
	digitalWrite(_driverLogicPin, HIGH);
	digitalWrite(_dir2Pin, dir);
	delay(10);
	for (int i = 0; i < steps; i++) {
		digitalWrite(_step2Pin, LOW);
		delayMicroseconds(_mdly);
		digitalWrite(_step2Pin, HIGH);
		delayMicroseconds(_mdly);
	}
}

void SatelliteAntenna::MoveAzimuthNorth() {
	WaitForVolts(_battFull);
	MoveDishEndstop();
	MoveDishSteps(_maxDishSteps /2);
	WaitForVolts(_battFull);
	bool stop_find = false;
	int inc = 2;
	while (!stop_find) {
		delay(1);
		float d = GetDir();
		if (d == 360.00 || d == 0.00) {
			stop_find = true;
		}
		else {
			if (d > 355 || d < 5) {
				inc = 2;
			}
			else {
				inc = 64;
			}
			LcdPrint("Finding North: " + FloatToString(d));
			if (d > 180) {
				MoveAzimuthSteps(inc, LOW);
			}
			else {
				MoveAzimuthSteps(inc, HIGH);
			}
		}
	}
	LcdPrint("N");
	MoveDishEndstop();
}

void SatelliteAntenna::MoveAzimuthSouth() {
	WaitForVolts(_battFull);
	MoveAzimuthSteps(500, LOW);
	MoveDishEndstop();
	MoveDishSteps(_maxDishSteps / 2);
	WaitForVolts(_battFull);
	bool stop_find = false;
	int inc = 2;
	while (!stop_find) {
		delay(1);
		float d = GetDir();
		if (d == 180) {
			stop_find = true;
		}
		else {
			if (d > 175 && d < 185) {
				inc = 2;
			}
			else {
				inc = 64;
			}
			LcdPrint("Finding South: " + FloatToString(d));
			if (d < 180) {
				MoveAzimuthSteps(inc, LOW);
			}
			else {
				MoveAzimuthSteps(inc, HIGH);
			}
		}
	}
	LcdPrint("S");
	MoveDishEndstop();
}

float SatelliteAntenna::GetDir() {
	float x = 0, y = 0;
	for (int i = 0; i < 4; i++) {
		delay(1);
		lsm.read();
		x += lsm.magData.x;
		y += lsm.magData.y;
	}
	x = x / 4;
	y = y / 4;
	float angX = map(x, _magMinX, _magMaxX, -90, 90);
	float angY = map(y, _magMinY, _magMaxY, -90, 90);
	float head = RAD_TO_DEG * (atan2(-angY, -angX) + PI);
	return head;
}

void SatelliteAntenna::LcdPrint(String text) {
	if (LcdText != text) {
		LcdText = text;
		display.setTextSize(1);
		display.setTextColor(WHITE);
		display.setCursor(0, 0);
		display.clearDisplay();
		display.display();
		display.print(text);
		delay(10);
		yield();
		display.display();
		delay(10);
	}
}

void SatelliteAntenna::LcdPrint(String text, String line2) {
	if (LcdText != text + line2) {
		LcdText = text + line2;
		display.setTextSize(1);
		display.setTextColor(WHITE);
		display.setCursor(0, 0);
		display.clearDisplay();
		display.display();
		display.println(text);
		display.println(line2);
		delay(10);
		yield();
		display.display();
		delay(10);
	}
}

void SatelliteAntenna::WaitForVolts(float v) {
	while (BatteryLevel() <= v) {
		digitalWrite(_driverLogicPin, LOW);
		while (gpsSerial.available() > 0)
			gps.encode(gpsSerial.read());

		if (gps.altitude.isUpdated()) {
			GpsMeters = String(gps.altitude.meters());
			GpsLat = String(gps.location.lat(), 6);
			GpsLng = String(gps.location.lng(), 6);

		}

		delay(100);
	}
}

float SatelliteAntenna::BatteryLevel() {
	float measuredvbat = analogRead(VBATPIN);
	measuredvbat *= 2;    // we divided by 2, so multiply back
	measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
	measuredvbat /= 1024; // convert to voltage

	LcdPrint("Batt=" + FloatToString(measuredvbat) + " Dir=" + FloatToString(GetDir()) + " ALT=" + GpsMeters + " LAT=" + GpsLat + " LNG=" + GpsLng);
	return measuredvbat;
}

String SatelliteAntenna::FloatToString(float f) {
	char temp[10];
	dtostrf(f, 1, 2, temp);
	return String(temp);
}


void SatelliteAntenna::configureUblox(byte* settingsArrayPointer)
{
	byte gpsSetSuccess = 0;
	//Serial.println("Configuring u-Blox GPS initial state...");

	//Generate the configuration string for Navigation Mode
	byte setNav[] = { 0xB5, 0x62, 0x06, 0x24, 0x24, 0x00, 0xFF, 0xFF, *settingsArrayPointer, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00, 0x05, 0x00, 0xFA, 0x00, 0xFA, 0x00, 0x64, 0x00, 0x2C, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	calcChecksum(&setNav[2], sizeof(setNav) - 4);

	//Generate the configuration string for Data Rate
	byte setDataRate[] = { 0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, settingsArrayPointer[1], settingsArrayPointer[2], 0x01, 0x00, 0x01, 0x00, 0x00, 0x00 };
	calcChecksum(&setDataRate[2], sizeof(setDataRate) - 4);

	//Generate the configuration string for Baud Rate
	byte setPortRate[] = { 0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00, settingsArrayPointer[3], settingsArrayPointer[4], settingsArrayPointer[5], 0x00, 0x07, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	calcChecksum(&setPortRate[2], sizeof(setPortRate) - 4);

	byte setGLL[] = { 0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x2B };
	byte setGSA[] = { 0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x32 };
	byte setGSV[] = { 0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x39 };
	byte setRMC[] = { 0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x04, 0x40 };
	byte setVTG[] = { 0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x46 };

	delay(2500);

	while (gpsSetSuccess < 3)
	{
		LcdPrint("Setting Navigation Mode... ");
		sendUBX(&setNav[0], sizeof(setNav));  //Send UBX Packet
		gpsSetSuccess += getUBX_ACK(&setNav[2]); //Passes Class ID and Message ID to the ACK Receive function
		if (gpsSetSuccess == 5)
		{
			gpsSetSuccess -= 4;
			setBaud(settingsArrayPointer[4]);
			delay(1500);
			byte lowerPortRate[] = { 0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00, 0x80, 0x25, 0x00, 0x00, 0x07, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0xA2, 0xB5 };
			sendUBX(lowerPortRate, sizeof(lowerPortRate));
			gpsSerial.begin(9600);
			delay(2000);
		}
		if (gpsSetSuccess == 6) gpsSetSuccess -= 4;
		if (gpsSetSuccess == 10) gpsStatus[0] = true;
	}
	if (gpsSetSuccess == 3) LcdPrint("Navigation mode configuration failed.");
	gpsSetSuccess = 0;
	while (gpsSetSuccess < 3)
	{
		LcdPrint("Setting Data Update Rate... ");
		sendUBX(&setDataRate[0], sizeof(setDataRate));  //Send UBX Packet
		gpsSetSuccess += getUBX_ACK(&setDataRate[2]); //Passes Class ID and Message ID to the ACK Receive function      
		if (gpsSetSuccess == 10) gpsStatus[1] = true;
		if (gpsSetSuccess == 5 | gpsSetSuccess == 6) gpsSetSuccess -= 4;
	}
	if (gpsSetSuccess == 3) LcdPrint("Data update mode configuration failed.");
	gpsSetSuccess = 0;


	while (gpsSetSuccess < 3 && settingsArrayPointer[6] == 0x00)
	{
		LcdPrint("Deactivating NMEA GLL Messages ");
		sendUBX(setGLL, sizeof(setGLL));
		gpsSetSuccess += getUBX_ACK(&setGLL[2]);
		if (gpsSetSuccess == 10) gpsStatus[2] = true;
		if (gpsSetSuccess == 5 | gpsSetSuccess == 6) gpsSetSuccess -= 4;
	}
	if (gpsSetSuccess == 3) LcdPrint("NMEA GLL Message Deactivation Failed!");
	gpsSetSuccess = 0;

	while (gpsSetSuccess < 3 && settingsArrayPointer[7] == 0x00)
	{
		LcdPrint("Deactivating NMEA GSA Messages ");
		sendUBX(setGSA, sizeof(setGSA));
		gpsSetSuccess += getUBX_ACK(&setGSA[2]);
		if (gpsSetSuccess == 10) gpsStatus[3] = true;
		if (gpsSetSuccess == 5 | gpsSetSuccess == 6) gpsSetSuccess -= 4;
	}
	if (gpsSetSuccess == 3) LcdPrint("NMEA GSA Message Deactivation Failed!");
	gpsSetSuccess = 0;

	while (gpsSetSuccess < 3 && settingsArrayPointer[8] == 0x00)
	{
		LcdPrint("Deactivating NMEA GSV Messages ");
		sendUBX(setGSV, sizeof(setGSV));
		gpsSetSuccess += getUBX_ACK(&setGSV[2]);
		if (gpsSetSuccess == 10) gpsStatus[4] = true;
		if (gpsSetSuccess == 5 | gpsSetSuccess == 6) gpsSetSuccess -= 4;
	}
	if (gpsSetSuccess == 3) LcdPrint("NMEA GSV Message Deactivation Failed!");
	gpsSetSuccess = 0;

	while (gpsSetSuccess < 3 && settingsArrayPointer[9] == 0x00)
	{
		LcdPrint("Deactivating NMEA RMC Messages ");
		sendUBX(setRMC, sizeof(setRMC));
		gpsSetSuccess += getUBX_ACK(&setRMC[2]);
		if (gpsSetSuccess == 10) gpsStatus[5] = true;
		if (gpsSetSuccess == 5 | gpsSetSuccess == 6) gpsSetSuccess -= 4;
	}
	if (gpsSetSuccess == 3) LcdPrint("NMEA RMC Message Deactivation Failed!");
	gpsSetSuccess = 0;

	while (gpsSetSuccess < 3 && settingsArrayPointer[10] == 0x00)
	{
		LcdPrint("Deactivating NMEA VTG Messages ");
		sendUBX(setVTG, sizeof(setVTG));
		gpsSetSuccess += getUBX_ACK(&setVTG[2]);
		if (gpsSetSuccess == 10) gpsStatus[6] = true;
		if (gpsSetSuccess == 5 | gpsSetSuccess == 6) gpsSetSuccess -= 4;
	}
	if (gpsSetSuccess == 3) LcdPrint("NMEA VTG Message Deactivation Failed!");

	gpsSetSuccess = 0;
	if (settingsArrayPointer[4] != 0x25)
	{
		LcdPrint("Setting Port Baud Rate... ");
		sendUBX(&setPortRate[0], sizeof(setPortRate));
		setBaud(settingsArrayPointer[4]);
		//LcdPrint("Success!");
		delay(500);
	}
}


void SatelliteAntenna::calcChecksum(byte* checksumPayload, byte payloadSize)
{
	byte CK_A = 0, CK_B = 0;
	for (int i = 0; i < payloadSize; i++)
	{
		CK_A = CK_A + *checksumPayload;
		CK_B = CK_B + CK_A;
		checksumPayload++;
	}
	*checksumPayload = CK_A;
	checksumPayload++;
	*checksumPayload = CK_B;
}

void SatelliteAntenna::sendUBX(byte* UBXmsg, byte msgLength)
{
	for (int i = 0; i < msgLength; i++)
	{
		gpsSerial.write(UBXmsg[i]);
		gpsSerial.flush();
	}
	gpsSerial.println();
	gpsSerial.flush();
}


byte SatelliteAntenna::getUBX_ACK(byte* msgID)
{
	byte CK_A = 0, CK_B = 0;
	byte incoming_char;
	boolean headerReceived = false;
	unsigned long ackWait = millis();
	byte ackPacket[10] = { 0xB5, 0x62, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	int i = 0;
	while (1)
	{
		if (gpsSerial.available())
		{
			incoming_char = gpsSerial.read();
			if (incoming_char == ackPacket[i])
			{
				i++;
			}
			else if (i > 2)
			{
				ackPacket[i] = incoming_char;
				i++;
			}
		}
		if (i > 9) break;
		if ((millis() - ackWait) > 1500)
		{
			//LcdPrint("ACK Timeout");
			return 5;
		}
		if (i == 4 && ackPacket[3] == 0x00)
		{
			LcdPrint("NAK Received");
			return 1;
		}
	}

	for (i = 2; i < 8; i++)
	{
		CK_A = CK_A + ackPacket[i];
		CK_B = CK_B + CK_A;
	}
	if (msgID[0] == ackPacket[6] && msgID[1] == ackPacket[7] && CK_A == ackPacket[8] && CK_B == ackPacket[9])
	{
		//Serial.println("Success!");
		LcdPrint("ACK Received! ");
		printHex(ackPacket, sizeof(ackPacket));
		return 10;
	}
	else
	{
		LcdPrint("ACK Checksum Failure: ");
		printHex(ackPacket, sizeof(ackPacket));
		delay(1000);
		return 1;
	}
}


void SatelliteAntenna::printHex(uint8_t* data, uint8_t length) // prints 8-bit data in hex
{
	char tmp[length * 2 + 1];
	byte first;
	int j = 0;
	for (byte i = 0; i < length; i++)
	{
		first = (data[i] >> 4) | 48;
		if (first > 57) tmp[j] = first + (byte)7;
		else tmp[j] = first;
		j++;

		first = (data[i] & 0x0F) | 48;
		if (first > 57) tmp[j] = first + (byte)7;
		else tmp[j] = first;
		j++;
	}
	tmp[length * 2] = 0;
	String rsp = "";
	for (byte i = 0, j = 0; i < sizeof(tmp); i++)
	{
		//LcdPrint(tmp[i]);
		rsp = rsp + tmp[i];
		if (j == 1)
		{
			rsp = rsp + " ";//Serial.print(" ");
			j = 0;
		}
		else j++;
	}
	LcdPrint(rsp);
	//Serial.println();
}

void SatelliteAntenna::setBaud(byte baudSetting)
{
	if (baudSetting == 0x12) gpsSerial.begin(4800);
	if (baudSetting == 0x4B) gpsSerial.begin(19200);
	if (baudSetting == 0x96) gpsSerial.begin(38400);
	if (baudSetting == 0xE1) gpsSerial.begin(57600);
	if (baudSetting == 0xC2) gpsSerial.begin(115200);
	if (baudSetting == 0x84) gpsSerial.begin(230400);
}